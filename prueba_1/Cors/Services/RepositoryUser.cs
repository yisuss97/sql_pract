﻿using Dapper;
using Microsoft.Data.SqlClient;
using prueba_1.Models;

namespace prueba_1.Cors.Services
{

    public class RepositoryUser
    {
        private readonly string connectionString;
        public RepositoryUser(IConfiguration configuration)
        {
            connectionString = configuration.GetConnectionString("DefaultConnection");
        }
        public async Task<Usuario> FindUser(String email,String password)
        {
            var id = 1;
            var connection = new SqlConnection(connectionString);
            var user = connection.Query<Usuario>(@"SELECT * FROM [Iberia].[dbo].[TCScaf_Usuario] WHERE tx_correo=@Email AND tx_contrasena=@Password",
                new {email,password}).FirstOrDefault();
            user.Rol=  connection.Query<string>(@"SELECT tx_descripcionRolUsuario FROM [Iberia].[dbo].[TCScaf_RolUsuario] WHERE cd_identityRolUsuario=1 ").FirstOrDefault();
            return user;
        }
    

    }
}
