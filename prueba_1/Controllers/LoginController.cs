﻿using Dapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using prueba_1.Cors.Services;
using prueba_1.Models;

namespace prueba_1.Controllers
{
    public class LoginController : Controller
    {
        private readonly RepositoryUser repositoryUser;
        private  Usuario usuario1;

        public  LoginController(RepositoryUser repository)
        {
            repositoryUser=repository;
        }

           
        public IActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Login(Usuario usuario)
        {
            if (!ModelState.IsValid)
            {
                
                return View(usuario);
            }
            var us = await repositoryUser.FindUser(usuario.Email, usuario.Password);
            
            return  RedirectToAction("Home", "Login", us);
        }
        public IActionResult Home(Usuario usuarios)
        {
     
            return View(usuarios);
        }

    }
}
