﻿using System.ComponentModel.DataAnnotations;

namespace prueba_1.Models
{
    public class Usuario
    {
        public int Id { get; set; }
        public string cd_usuario { get; set; } = "";
        [Required(ErrorMessage ="Password Requerido")]
        [StringLength(maximumLength:20,MinimumLength = 2, ErrorMessage = "Password invalido")]
        public string Password { get; set; } = "";
        [Required(ErrorMessage = "Email Requerido")]
        public string Email { get; set; } = "";

        public string nb_nombre { get; set; } = "";
        public string cd_identityRolUsuario { get; set; } = "";
        public string Rol { get; set; } = "";

    }
}
